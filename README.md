# ATLAS Analysis Docker Images with Kaniko in GitLab

## Overview

This repository demonstrates the method of running Docker images through GitLab CI/CD using Kaniko. It's specifically set up for ATLAS analysis but is adaptable for any similar analysis projects on GitLab.

## Repository Structure

The repository is configured to trigger the building of two different Docker images located in two distinct directories: `FinalSelection` and `likfit`. This process is managed through the `.gitlab-ci.yml` file at the root of the repository.

## Requirements

- GitLab account
- Access to a GitLab project
- Basic understanding of Docker and GitLab CI/CD

## `.gitlab-ci.yml` Configuration

The `.gitlab-ci.yml` file contains the configuration for two separate build jobs: `build_kaniko_command` and `build_second_image`. Both jobs follow a similar structure but are designed to build Docker images from different directories.

### Job: `build_kaniko_command`

This job builds a Docker image from the `FinalSelection` directory. The key steps in the script are:

1. Prepare the Kaniko configuration file.
2. Build and push the Docker image using Kaniko executor.
3. Print the registry path of the pushed image.

### Job: `build_second_image`

This job is set up for the `likfit` directory, following similar steps as `build_kaniko_command`.

## Using Kaniko for Docker Builds

Kaniko is a tool to build Docker images from a Dockerfile, inside a container or Kubernetes cluster. It doesn't require Docker daemon, making it secure and efficient for CI/CD pipelines.

### Steps in Kaniko Build:

1. **Kaniko Configuration**: Authentication for the Docker registry is configured using a JSON config file.
2. **Image Building and Pushing**: The Kaniko executor is used to build the image from a specified Dockerfile and context directory, then push it to the defined registry.
3. **Image Tagging**: By default, images are tagged as `latest`. This can be customized in the `variables` section of each job.

## Advantages for ATLAS Analysis

- **Security**: Kaniko doesn't require Docker daemon, enhancing security.
- **Simplicity**: The process simplifies the building and pushing of Docker images in CI/CD pipelines.
- **Customization**: Easy to adapt for various directories and Dockerfile configurations.

